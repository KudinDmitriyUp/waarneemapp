<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents('database/data/data.json');
        $data = json_decode($json);
        foreach ($data as $objs){
            foreach($objs as $obj){
                $user = User::firstOrCreate([
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'age' => $obj->age
                ]);
               // dd($obj->id);
                foreach($obj->companies as $company){
                    $newCompany = Company::firstOrCreate([
                        'id' => $company->id,
                        'name' => $company->name,
                        'started_at' => $company->started_at
                    ]);
                    $user->companies()->attach($company->id);
                }
            }
        }
    }
}
