<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function getCompanies(Request $request)
    {
        $maxAge = $request->input('maxAge');
        $minAge = $request->input('minAge');
        return Company::whereHas('users', function (Builder $query) use ($maxAge, $minAge) {
            $query->where('age', '>=', $minAge)->where('age', '<=', $maxAge);//->get(['users.id', 'users.name', 'users.age'])
        })->with('users:id,name,age')->get(['id', 'name', 'started_at']);
    }
}
